import { LightningElement, track, api } from 'lwc';

export default class CheckboxButtonLwc extends LightningElement {
    
    @api selected = false;
    @api selectedIcon = 'utility:check';
    @api deselectedIcon = 'utility:add';
    @track selectedLabelClass = 'slds-checkbox-button slds-checkbox-button_is-checked';
    @track deselectedLabelClass = 'slds-checkbox-button';
    @track iconName = '';
    @track labelName = '';
    @track hasRendered = false;

    renderedCallback() {
        if (this.hasRendered == false) {
            this.hasRendered = true;
            this.toggleIconDetails();
        }
    }

    handleClick(event) {
        this.selected = (this.selected == true) ? false : true;
        this.toggleIconDetails();
    }
    
    toggleIconDetails() {
        if (this.selected == true) {
            this.iconName = this.selectedIcon;
            this.labelName = this.selectedLabelClass;
        } else {
            this.iconName = this.deselectedIcon;
            this.labelName = this.deselectedLabelClass;
        }
    }
}